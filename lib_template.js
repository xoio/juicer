let imports = {};
imports['__wbindgen_placeholder__'] = module.exports;
let wasm;
import {TextDecoder, TextEncoder} from "util"
import path from "path"
import fs from "fs"

INSERT

const fpath = path.join(__dirname, 'glsl_wgsl_bg.wasm');
const fbytes = fs.readFileSync(fpath);

const wasmModule = new WebAssembly.Module(fbytes);
const wasmInstance = new WebAssembly.Instance(wasmModule, imports);
wasm = wasmInstance.exports;
module.exports.__wasm = wasm;

