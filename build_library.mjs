import fs from "fs"
import {spawn} from "node:child_process";

try {
    process.chdir("./wasm")
} catch (e) {
    throw new Error("Did you clone the wasm branch from the repo?")
}

let ls = spawn("wasm-pack", ["build", "--target", "nodejs" , "--release"])

// not sure why but stdout is getting piped through stderr here
ls.stderr.on("data", d => {
    console.log(d.toString())
})

ls.on("close", code => {

    process.chdir("../")
    let file = fs.readFileSync("./wasm/pkg/glsl_wgsl.js", "utf8")
    let template = fs.readFileSync("./lib_template.js", "utf8")


    // Extract the code we actually need to change.
    let start_index = 0;
    let end_index = 0;
    let lines = file.split("\n")

    lines.forEach((line, i) => {
        if (line === "const { TextDecoder, TextEncoder } = require(`util`);") {
            start_index = i + 1
        }

        if (line === "const path = require('path').join(__dirname, 'glsl_wgsl_bg.wasm');") {
            end_index = i - 1
        }
    })

    const extracted = lines.slice(start_index, end_index).join("\n")

    // insert extracted content into template
    template = template.replace("INSERT", extracted)

    fs.writeFileSync("./dist/glsl_wgsl.js", template)

    // update wasm binary
    fs.copyFileSync("./wasm/pkg/glsl_wgsl_bg.wasm", "./dist/glsl_wgsl_bg.wasm")

    // copy type definitions
    fs.copyFileSync("./wasm/pkg/glsl_wgsl.d.ts", "./dist/glsl_wgsl.d.ts")
    fs.copyFileSync("./wasm/pkg/glsl_wgsl_bg.wasm.d.ts", "./dist/glsl_wgsl_bg.wasm.d.ts")


})