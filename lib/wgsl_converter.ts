import {spawn, exec} from "node:child_process"
import {readFileSync, writeFileSync} from "fs"
import {resolve} from "path";
import {rm, rmSync} from "node:fs";

export default async function processWGSL(
    {
        shader = null,
        file_path = null,
        root_dir = resolve(process.cwd(), "src", "shaders"),
        type = ".vert"
    } = {}
) {

    if (!shader || !file_path) {
    }

    // First check to see if naga can be run.
    let canRun = true
    await new Promise((res, rej) => {
        // first test to see if naga can be run
        const ls = spawn("naga")
        let canRun = true

        ls.on("error", (e) => {
            // TODO not sure if there's a way to just read the error code, for now this is fine
            if (e.message === "spawn naga ENOENT") {
                canRun = false
                rej(`WGSL was specified with ${file_path} and Naga appears to be uninstalled.\n See https://github.com/gfx-rs/wgpu/tree/trunk/naga`)
            }
        })


        ls.on("close", (e) => {

            if (!canRun) {
                rej("unable to run Naga")
            } else {
                res(0)
            }
        })

    }).catch(e => {
        canRun = false
        console.error(e)
    })

    // if we can't, just return the shader as-is and make a note
    if (!canRun) {
        console.info("Unable to run Naga - please make sure it's installed")
        return shader
    }

    // Naga unfortunately requires files as input, so we need to temporarily write some files during this process
    // Will write to your root shader directory.
    // TODO it doesn't look very difficult to take source as an input/output so it might be worth doing down the line.
    let id = Math.floor(Math.random() * 999999999.0)
    let tmp = resolve(root_dir, `tmp${id}.${type}`)
    let out = resolve(root_dir, `out${id}.${type}.wgsl`)

    writeFileSync(tmp, shader)

    let convert = await new Promise((res, rej) => {
        exec(`naga ${tmp} ${out}`, (err, stdout, stderr) => {
            if (err) {

                rej(stderr)
            } else {
                res(readFileSync(out, "utf-8"))
            }
        })
    }).catch(e => {
        console.log("Something went wrong during conversion - ", e)
    })

    if (convert === undefined || convert === null) {
        convert = ""
    }

    // remove tmp files, can run async since we're not waiting on input.
    rm(tmp, (err) => {
        if (err) {
            console.error("Error removing temporary shader source file")
        }
    })
    rm(out, (err) => {
        if (err) {
            console.error("Error removing temporary compiled wgsl file")
        }
    })


    return convert
}
