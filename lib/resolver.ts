import fs from "fs"
import {resolve, dirname} from "path"

/**
 * Resolves any include files
 * @param path {string} path to the current file
 * @param current_dir {string} path to the current directory
 * @param output {Array<string>} array containing the final contents after resolving all possible include paths.
 */
export default function resolveIncludes(path: string, current_dir: string, output: Array<string>) {

    let content = fs.readFileSync(path, "utf-8")
    let lines = content.split("\n")

    lines.forEach((line, i) => {
        if (line.search("#include") !== -1) {
            let path = line.replace("#include ", "")
            let ipath = resolve(current_dir, path)
            let parent = dirname(ipath)
            // not sure why there's an added return line but try to replace
            ipath = ipath.replace("\r", "")

            resolveIncludes(ipath, parent, output)

        } else {
            output.push(line + "\n")
        }
    })

    return output.join("\n")

}

function buildFriendlyPath(path) {
    path = path.split("\\")
    console.log(path)
}

