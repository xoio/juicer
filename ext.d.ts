declare module '*.glsl' {
    const shader: string;
    // @ts-ignore
    export default shader;
}

declare module '*.wgsl' {
    const shader: string;
    // @ts-ignore
    export default shader;
}