Juicer
===
A simple shader concatenation helper in the form of a Vite plugin.
It will also attempt to convert GLSL to WGSL using [Naga](https://github.com/gfx-rs/wgpu/tree/trunk/naga)

To use
===
* You can use an `#include` statement in your shader and the plugin will automatically resolve and read the file, replacing the
  `#include` statement with the file contents.
* By default, the plugin expects your shader folder to be at `src/shaders` but you can change that behavior by passing in `shader_root` as a plugin parameter
  with wherever your shader folder is.

WGSL
===
WGSL conversion is facilitated by Naga which is part of the `wgpu` library. This means you will need
[Rust](https://www.rust-lang.org/). This was last tested against 1.74.1

* For WGSL conversion, your files must be in one of the following formats
    * `.wgsl.vert`
    * `.wgsl.frag`
* Also note that your glsl must be version 440 or higher.


### 🛠️ Test locally `cargo run --target <target arch>`

### 🛠️ Build with `wasm-pack build`

```
wasm-pack build
```

