/* tslint:disable */
/* eslint-disable */
/**
* @param {string} content
* @param {number} shader_type
* @returns {string}
*/
export function process_file(content: string, shader_type: number): string;
